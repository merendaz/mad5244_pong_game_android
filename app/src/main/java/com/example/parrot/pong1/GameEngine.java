package com.example.parrot.pong1;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.constraint.solver.widgets.Rectangle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    // -----------------------------------
    // ## ANDROID DEBUG VARIABLES
    // -----------------------------------

    // Android debug variables
    final static String TAG="PONG-GAME";

    // -----------------------------------
    // ## SCREEN & DRAWING SETUP VARIABLES
    // -----------------------------------

    // screen size
    int screenHeight;
    int screenWidth;

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;

    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;

    // -----------------------------------
    // ## GAME SPECIFIC VARIABLES
    // -----------------------------------

    // ----------------------------
    // ## SPRITES
    // ----------------------------

    // Position of the ball (Circle):
    int ballCircleXPosition;//keep track of ball => x axis
    int ballCircleYPosition;//keep track of ball => y axis

    // Position of the racket (Rect):
    int racketXPosition;//keep track of ball => x axis
    int racketYPosition;//keep track of ball => y axis

    // Velocity of the ball:
    int vx; // Horizontal Movement
    int vy; // Vertical Movement

    // Velocity of the racket:
    int vrx; // Horizontal Movement

    // Direction of racket movement:
    String racketDir = "";

    String directionCircleBallIsMoving = "";


    // ----------------------------
    // ## GAME STATS - number of lives, score, etc
    // ----------------------------

    int lives;
    int score;

    public GameEngine(Context context, int w, int h) {
        super(context);

        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;


        this.printScreenInfo();

        // @TODO: Add your sprites to this section
        // This is optional. Use it to:
        //  - setup or configure your sprites
        //  - set the initial position of your sprites

        resetGame();
//        this.racketXPosition = this.screenWidth / 2 - 125;
//        this.racketYPosition = this.screenHeight - 260;
//
//        this.ballCircleXPosition = this.screenWidth / 2;
//        this.ballCircleYPosition = this.screenHeight - 280;
        // @TODO: Any other game setup stuff goes here
    }

    // ------------------------------
    // HELPER FUNCTIONS
    // ------------------------------

    // This funciton prints the screen height & width to the screen.
    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }


    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    public void resetGame() {
        // Velocity of the racket:
        this.vrx = 20; // Horizontal Movement

        // Direction of racket movement:
       this.racketDir = "";

        // Direction of ball movement:
       directionCircleBallIsMoving = "down";

        // ----------------------------
        // ## GAME STATS - number of lives, score, etc
        // ----------------------------

        this.lives = 3;
        this.score = 25;
        this.racketXPosition = this.screenWidth / 2 - 125;
        this.racketYPosition = this.screenHeight - 260;

        this.ballCircleXPosition = this.screenWidth / 2;
        this.ballCircleYPosition = 0;

    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    // 1. Tell Android the (x,y) positions of your sprites
    public void updatePositions() {
        // @TODO: Update the position of the sprites

        if (this.lives == 0) {
            resetGame();
            pauseGame();
        }
        // Updating the position of the Circle Ball:
        if (directionCircleBallIsMoving == "up") {
            this.ballCircleYPosition = this.ballCircleYPosition - 40;

            if (this.ballCircleYPosition < 50) {
                directionCircleBallIsMoving = "down";
            }

        }
        else if (directionCircleBallIsMoving == "down") {
            this.ballCircleYPosition = this.ballCircleYPosition + 40;
            if ((this.ballCircleYPosition > this.racketYPosition - 90)
                    && (this.ballCircleXPosition < this.racketXPosition + 250)
                    && (this.ballCircleXPosition > this.racketXPosition)) {
                Log.d(TAG, "Ball touched racket on YPosition = " + this.ballCircleYPosition);
                this.score += 1;
                directionCircleBallIsMoving = "up";
            }
            if (this.ballCircleYPosition > this.screenHeight - 250) {
                Log.d(TAG, "Ball touched the Floor");
                this.lives -= 1;
                directionCircleBallIsMoving = "up";
            }
        }
        if ((this.racketXPosition + 250) > this.screenWidth) {
            Log.d(TAG, "Racket on the Right Wall");
            this.racketXPosition = this.screenWidth - 250;
        }

        if ((this.racketXPosition) < 0) {
            Log.d(TAG, "Racket on the Left Wall");
            this.racketXPosition = 0;
        }
        if (this.racketDir.contentEquals("left")){
            this.racketXPosition -= vrx;
        }
        else if (this.racketDir.contentEquals("right")){
            this.racketXPosition += vrx;
        }
// DEBUG - by outputing current positiong

        Log.d(TAG, "XPos: " + this.ballCircleXPosition);

    // @TODO: Collision detection code

    }
    // 2. Tell Android to DRAW the sprites at their positions
    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------
            // Put all your drawing code in this section

            // configure the drawing tools
            this.canvas.drawColor(Color.argb(255,255,0,0));
            paintbrush.setColor(Color.WHITE);


            //@TODO: Draw the sprites (rectangle, circle, etc)

//            // 1. Draw the ball
//            this.canvas.drawRect(
//                    ballXPosition,
//                    ballYPosition,
//                    ballXPosition + 50,
//                    ballYPosition + 50,
//                    paintbrush);


            // 1. Drawing a Filled Circle => The Ball
//            paintbrush.setStyle(Paint.Style.FILL);
            paintbrush.setColor(Color.YELLOW);
            canvas.drawCircle(this.ballCircleXPosition, this.ballCircleYPosition,25, paintbrush);

            paintbrush.setColor(Color.WHITE);

            // ==============================
            // 1. Draw the racket
            // ==============================
            this.canvas.drawRect(
                    racketXPosition,
                    racketYPosition - 40,
                    racketXPosition + 250,
                    racketYPosition + 10,
                    paintbrush);


            paintbrush.setColor(Color.WHITE);
            //@TODO: Draw game statistics (lives, score, etc)
            paintbrush.setTextSize(60);
            canvas.drawText("Score: " + this.score, 20, 100, paintbrush);
            canvas.drawText("Lifes remaining: " + lives, 550, 100, paintbrush);

            //----------------
            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    // Sets the frame rate of the game
    public void setFPS() {
        try {
            gameThread.sleep(50);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_DOWN) {
            // user pushed down on screen
            float fingerXPosition = event.getX();
            float fingerYPosition = event.getY();
            if (this.gameIsRunning == false) {
                startGame();
            }
            // Works but the resulting movement is not natural
            // this.racketXPosition = (int) fingerXPosition;
            if (fingerXPosition < this.screenWidth/2){
                this.racketDir = "left";
            }
            else if (fingerXPosition > this.screenWidth/2){
                this.racketDir = "right";
            }
        }
        else if (userAction == MotionEvent.ACTION_UP) {
            // user lifted their finger
            // Didn't work, because we have to "click" the trackpad everytime, maybe in touch?!
           // this.racketDir = "";
        }
        return true;
    }
}